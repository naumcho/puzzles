import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

//import org.junit.Assert;
//import org.junit.Test;


/**
 * Simple program that finds if two cities are connected in a graph
 * given a list of edges between individual cities
 * 
 * @author Naoum Naoumov
 *
 */

public class Connected {

    /**
     * A generic Node class that has a name - can represent cities etc.
     * 
     * This class is a key in a hashmap so needs hashCode and equals
     * @author root
     *
     */
    public static class Node {
		private String name;

        public Node(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Node [name=" + name + "]";
        }

        @Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Node other = (Node) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}


    }
    
    /**
     * A graph that maintains an adjacency list for each node and a map to get the nodes by name
     * @author root
     *
     */
    public static class Graph {
        //graph adjancency list
        private Map<Node, ArrayList<Node>> adjList = new HashMap<Node, ArrayList<Node>>();
        //city name -> Node representing the city
        private Map<String, Node> nodeMap = new HashMap<String, Node>();

        /**
         * Find the Node in the map or insert it if it's not there already
         * @param name
         * @return
         */
        private Node makeGetNode(String name) {
             Node n = nodeMap.get(name);
             if (n == null) {
                 n = new Node(name);
                 nodeMap.put(name, n);
             }
             return n;
        }

        /**
         * Build the graph from a file with each line having format: <src>, <dst>
         * @param filename
         * @return 
         * @throws IOException
         */
        public Graph build(String filename) throws IOException {
            Graph g = new Graph();
            String line;
            BufferedReader br = new BufferedReader(new FileReader(filename));
               while ((line = br.readLine()) != null) {
                 String[] parts = line.split(","); //only works with ", " between cities
                 assert(parts.length == 2);
                 
                 //does java not have a map[] operator?
                 Node src = makeGetNode(parts[0].trim());
                 Node dst = makeGetNode(parts[1].trim());

                 //Since this is an undirected graph we add both src->dst and dst->src
                 addEdge(src,dst);
                 addEdge(dst,src);
               }
            return g; 
        }
        
        private void addEdge(Node src, Node dst) {
             ArrayList<Node> list = adjList.get(src);
             if (list == null) {
                 list = new ArrayList<Node>();
                 adjList.put(src, list);
             }
             list.add(dst);
        }
        
        /**
         * This is a modified version of BFS that exits as soon as it finds target
         * 
         * @param source
         * @param target
         * @return
         */
        public boolean isConnected(String source, String target) {

            Set<Node> visited = new HashSet<Node>();
            List<Node> queue = new LinkedList<Connected.Node>();
            
            Node src = getNode(source);
            if (src == null) return false;
            Node dst = getNode(target);
            if (dst == null) return false;
//          System.out.println("checking: " + src.getName() + " -> " + dst.getName());
            
            queue.add(src);
            while (! queue.isEmpty() ) {
                Node n = queue.get(0); queue.remove(0); //cleaner Queue interface?
                visited.add(n);
                
                ArrayList<Node> neighbours = adjList.get(n);
                if (neighbours != null) {
                for (Node r : neighbours) {
                    if (! visited.contains(r) ) {
//                      System.out.println(r.getName());
                        //if we find the node we don't need to process the rest of the graph
                        if (r == dst) return true; 
                        queue.add(r);
                    }
                }
                }
            }
            return false;
        }

        public Map<Node, ArrayList<Node>> getAdjList() {
            return adjList;
        }

        public Map<String, Node> getNodeMap() {
            return nodeMap;
        }
        public Node getNode(String name) {
            return nodeMap.get(name);
        }

    }
    
    

    /**
     * @param args: <filename> <cityname1> <cityname2>
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            System.err.println("Usage: Connected <cities_file> <source_city> <target_city>");
            System.exit(1);
        }
        
        Graph g = new Graph();
        g.build(args[0]);
        
        boolean connected = g.isConnected(args[1], args[2]);
        System.out.println(connected ? "yes" : "no");       
    }

    

    @Test
    public void testBuildGraph() throws IOException  {
        Graph g = new Graph();;
        g.build("cities.txt");

        Assert.assertEquals(9, g.getAdjList().size());
        Assert.assertEquals(9, g.getNodeMap().size());
        Assert.assertEquals(2, g.getAdjList().get(g.makeGetNode("Philadelphia")).size());
        Assert.assertEquals(3, g.getAdjList().get(g.makeGetNode("New York")).size());
        
    }
    
    @Test
    public void testIsConnected() throws IOException  {      
        Graph g = new Graph();;
        g.build("cities.txt");
        Assert.assertTrue(g.isConnected("New York", "Boston"));
        Assert.assertTrue(g.isConnected("Boston", "Pittsburgh"));
        Assert.assertFalse(g.isConnected("Boston", "Tampa"));
        Assert.assertFalse(g.isConnected("Boston", "Ypsilanti"));
    }

}

