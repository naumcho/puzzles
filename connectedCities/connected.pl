#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;


die "Usage: $0 <filename> <source> <target>\n" unless @ARGV == 3;
my ($fn, $src, $dst) = @ARGV;

my $adjList = loadCities($fn);
#print Dumper($adjList);

print isConnected($adjList, $src, $dst) ? "yes\n" : "no\n";

sub isConnected {
    my ($adjList, $src, $dst) = @_;

    return 0 unless(exists $adjList->{$src} and exists $adjList->{$dst});
    return 1 if ($src eq $dst);

    #bfs implementation
    my %visited = ();
    my @queue = ($src);
    while (scalar @queue) {
        my $n = shift @queue;
        $visited{$n} = 1;

        foreach my $r (@{ $adjList->{$n} }) {
            if (! exists $visited{$r} ) {
                return 1 if ( $r eq $dst);
                push @queue, $r;
            }
        }
    }
    return 0;
}

sub loadCities {
    my $fn = shift;
    
    my %edges = ();
    open F, $fn or die "error reading $fn";
    foreach my $line (<F>) {
        chomp $line;
        my ($src, $dst) = split/, ?/, $line;
        push @{$edges{$src}}, $dst;
        push @{$edges{$dst}}, $src;
    }
    close F;
    return \%edges;
}


