/*
http://www.spotify.com/us/jobs/tech/zipfsong/
Naoum Naoumov naumcho@gmail.com
*/
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

//typedef pair<double, string> Song;
struct Song {
  Song(double f_, const string& name_) : name(name_), f(f_), z(0), q(0) {};
  static bool comp(const Song& l_, const Song& r_) { return l_.q > r_.q; }
  string name;
  double f;
  double z;
  double q;
};
typedef vector<Song> Album;

void getInputs(int &n, int &m, Album &album);
void print(const Album & a_); 
int main() {
  int n, m;
  Album album;
  getInputs(n, m, album);

  double topFreq = 1;
  for(size_t j = 0; j < album.size(); ++j) {
    Song &s  = album[j];
    s.z = topFreq / (j+1); //vector is 0 based
    s.q = s.f/s.z;
  }
  //print(album);

  stable_sort(album.begin(), album.end(), Song::comp);
  //print(album);

  for(int j = 0; j < m; j++) {
    cout << album[j].name << endl;
  }

  return 0;
}


void getInputs(int &n, int &m, Album &album) {
  cin >> n >> m;

  double i;
  string s;
  while (cin >> i >> s) {
 //   cerr << "|" << i << "|" << s << "|" << endl;
    album.push_back(Song(i, s));
  }
  //cerr << album.size() << endl;
}
void print(const Album & a_) {
  cerr << "##################" << endl;
  for(int i = 0; i < a_.size(); i++) {
    const Song &s  = a_[i];
    cerr << s.name << "|" << s.f << "|" << s.z << "|" << s.q << endl;
  }
  cerr << "##################" << endl;
}

