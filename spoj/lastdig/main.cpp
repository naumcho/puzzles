//http://www.spoj.com/problems/LASTDIG/
#include <iostream>
using namespace std;

void work() {
  int a, b;
  cin >> a >> b;


  int product = 1;
  b = b % 4 + (b > 0 ? 4 : 0);
  int last = a % 10;
//  if (b==0) product = 1;
//  else if (last == 1 || last == 5 || last == 0 || last == 6) product = last;
//  else 
  for (int i=0; i<b; i++) {
    product *= a; 
    product %= 10;
//    cout << product << "|" ;
  }

  cout << product << endl;
}

int main() {
  int tests = 0;
  cin >> tests;
  for (int n = 0; n < tests; n++) {
  work();
  }
}
