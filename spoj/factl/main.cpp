#include <iostream>
using namespace std;

int main() {

  int num;
  cin >> num;
  int div = 0;
  while ((num % 5) == 0) {
    div++;
    num /= 5;
  }
  cout << div;

}

