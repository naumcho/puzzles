#include <iostream>
using namespace std;

//(a+(b*c)) -> abc*+

struct node {
  node() : l(0), r(0), op(0) {}
  node * l;
  node * r;
  char op;
};

node* parse(string line, int& i) {
  node * n = new node();
//  cout << "parse" << endl;
  int side = 0;
  while (i < line.size()) { 
    bool exit = false;
    char chr = line[i++];
  //  cout << "looking at " << chr << endl;
    switch(chr) {
      case '(': 
	if (side++ ==0) n->l = parse(line,i); else n->r = parse(line,i);
	break;
      case ')': exit = true;	   break;
      case '+': 
      case '-':
      case '*':
      case '/':
      case '^':
      	n->op = chr;
	break;
      default: 
      	node* t = new node();
	t->op = chr;
	if (side++ ==0) n->l = t; else n->r = t;
	break;
    };
    if (exit) { 
      //cout << "return" << n->op << endl; 
      return n;
      }
  }
//  cout << "final return" << endl;
  return side > 2 ? n : n->l;
}
void print(node* n) {
  if (!n) return;
  print(n->l);
  print(n->r);
  cout << n->op;
}

int main() {
  int tests = 0;
  cin >> tests;
  for (int n = 0; n < tests; n++) {
    string line;
    cin >> line;

//    cout << line << endl;
    int i = 0;
    node * root = parse(line, i);
    print(root); cout <<endl;
  }
  return 0;
}
