//http://www.spoj.com/problems/SHPATH/
#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <queue>
#include <limits>
#include <set>
using namespace std;

typedef vector<pair<int,int> > AdjList;
struct Graph {
  void print() {
    for(int i = 0; i < edges.size(); i++) {
      cout << "V: " << i << ": " ;
    	for(int j = 0; j < edges[i].size(); j++) {
	  cout << "(" << edges[i][j].first << "," << edges[i][j].second <<")";
	}
	cout << endl;
    }
    cout << "Targets: " << paths.size() << " ";
    for(int i = 0; i < paths.size(); i++) 
      cout << paths[i].first <<"|"<<paths[i].second <<", ";
    cout << endl;
  }
  map<string, int> nameToIndex; 
  vector<AdjList> edges; //pair<index,weight>
  int numVerts;
  vector<pair<int,int> > paths;
};
  
void build_graph(Graph & g) {
  g.edges.push_back(AdjList());
  int numCities;
  cin >> numCities;
  for (int i =0; i < numCities; i++) {
    string city;
    cin >> city;
    g.nameToIndex[city] = i+1; 
    int peers;
    cin >> peers;
    AdjList adjList;
    for (int j = 0; j < peers; j++) {
      int idx, cost;
      cin >> idx >> cost;
      adjList.push_back(AdjList::value_type(idx,cost));
    }
    g.edges.push_back( adjList );
  }
  g.numVerts = numCities;

  int numTargets;
  cin >> numTargets;
  for(int i = 0; i< numTargets; i++) {
    string source, target;
    cin >> source >> target;

    int srcIdx = g.nameToIndex.find(source)->second;
    int targetIdx = g.nameToIndex.find(target)->second;
    g.paths.push_back(vector<pair<int,int> >::value_type(srcIdx,targetIdx));
  }
}

long long int shpath(Graph &g, int source, int target) {
  priority_queue<int, std::vector<int>, std::greater<int> > q;
  vector<long long int> dist(g.numVerts+1);
  for(int i =0; i< dist.size(); i++) dist[i] = numeric_limits<int>::max();
  set<int> visited;

  q.push(source);
  dist[source] = 0;

  while (!q.empty()) {
    int node = q.top(); q.pop(); visited.insert(node);
    //cout << "\t" << node << endl;
    AdjList& adj = g.edges[node];
    for(int i = 0; i < adj.size(); i++) {
      if (visited.find(adj[i].first) == visited.end()) q.push(adj[i].first);
      if (dist[adj[i].first] > dist[node] + adj[i].second) {
	dist[adj[i].first] = dist[node] + adj[i].second;
      }
    }
  }
  return dist[target];
}

void work() {

  Graph g;
  build_graph(g);
//  g.print();
  for(int i = 0; i < g.paths.size(); i++) {
    cout << shpath(g, g.paths[i].first, g.paths[i].second) << endl;
  }
}

int main() {
  int tests = 0;
  cin >> tests;
  for (int n = 0; n < tests; n++) {
  work();
  }
}
