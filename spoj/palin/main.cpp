#include <iostream>
using namespace std;
//http://www.spoj.com/problems/PALIN/

//increment the number upto index m
void inc(string & str, int m) {
  int carry  = 1;
  while (m >= 0 && carry) {
    if (str[m] == '9') {
      str[m] = '0';
      carry = 1;
    } else {
      str[m]++;
      carry = 0;
    }
    m--;
  }
  if (carry) str.insert(0,1,'1');
}

void work() {
  string str;
  cin >> str;
  //cout <<"orig " << str << endl;
  inc(str, str.length()-1);
  //cout <<"inc1 " << str << endl;
  {
    int len = str.length();
    int mid = len/2;
    bool even = (len % 2 == 0);

    bool toinc = false;
    for(int i = 0; i < mid; i++) {
      if (even) {
	if (str[mid+i] > str[mid-1-i]) { toinc = true; break; }
      } else {
	if (str[mid+1+i] > str[mid-1-i]) { toinc = true; break; }
      }
    }
    if (toinc) {
      //cout << "will incr" << endl;
      if (even) inc(str, mid-1);
      else inc(str, mid);
    }

    //cout << "incr " << str << endl;
  }

  int len = str.length();
  int mid = len/2;
  int even = (len % 2 == 0);

  //cout <<len <<mid<< "\t"<< str[mid] << endl;

  for(int i = 0; i < mid; i++) {
    if (even) {
      str[mid+i] = str[mid-1-i];
    } else {
      str[mid+1+i] = str[mid-1-i];
    }
  }

  cout << str << endl;

}

int main() {
  int num;
  cin >> num;
  for (int i = 0; i< num; i++) {
  work();
  }
  return 0;
}
