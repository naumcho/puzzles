#include <iostream>
#include <fstream>
using namespace std;

int main() {
  string line;
  int num;
  while(cin >> num) {
    if (num == 42) return 0;
    else cout << num << endl;
  }
  return 0;
}
