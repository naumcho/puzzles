#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

void dump(std::vector<bool> & v) {
  for(int i =0; i<v.size(); i++) cout << v[i];
  cout << endl;
}

int main() {
  long long m, n;
  int numTests;
//  cin >> numTests;
//  for (int i = 0; i< numTests; i++) {
    cin >> m >> n;
    std::vector<bool> isPrime(n-m);
    for(int j = 0; j< n+1; j++) if (j>=m) isPrime[j-m] = 1;
    //	dump(isPrime);

    int max = n+1;
    for(int j = 2; j< max; j++) {
      int step = j; 
      if (isPrime[j]) 
	while ((step+=j) < max) {
	  if (step>m) isPrime[step-m] = 0;    
	  //	dump(isPrime);
	}
      //dump(isPrime);
    }

    for (int p = m; p <n+1; p++) {
      if (p == 1) continue;
      if (isPrime[p-m]) cout << p << endl; 
    }
    cout << endl;
  //}

  return 0;
}
