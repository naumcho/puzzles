#include <iostream>
using namespace std;
//http://www.spoj.com/problems/JULKA/
//x + x + 3 = 10
//2x = 10 -3  => x = (10-y)/2

int main() {
  long long apples =0;
  long long diff =0;
  long long i = 0;
  while ( cin >> apples >> diff  && (++i < 10)) {
    long long ans = (apples -diff)/2;
  
    cout << ans + diff << endl << ans << endl;
  }
  return 0;
}
